package qb9.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import qb9.domain.enumeration.GiftPinType;

import qb9.domain.enumeration.GiftPinStatus;

/**
 * A GiftPin.
 */
@Entity
@Table(name = "tbl_giftpin")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class GiftPin implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "uid")
    private String uid;

    @Column(name = "pin_code")
    private String pinCode;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private GiftPinType type;

    @Column(name = "created_at")
    private ZonedDateTime createdAt;

    @Column(name = "updated_at")
    private ZonedDateTime updatedAt;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private GiftPinStatus status;

    @ManyToOne
    private Account account;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public GiftPin setUid(String uid) {
        this.uid = uid;
        return this;
    }

    public String getPinCode() {
        return pinCode;
    }

    public GiftPin setPinCode(String pinCode) {
        this.pinCode = pinCode;
        return this;
    }

    public GiftPinType getType() {
        return type;
    }

    public GiftPin setType(GiftPinType type) {
        this.type = type;
        return this;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public GiftPin setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public GiftPin setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public GiftPinStatus getStatus() {
        return status;
    }

    public GiftPin setStatus(GiftPinStatus status) {
        this.status = status;
        return this;
    }

    public Account getAccount() {
        return account;
    }

    public GiftPin setAccount(Account account) {
        this.account = account;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GiftPin giftPin = (GiftPin) o;
        if(giftPin.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, giftPin.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "GiftPin{" +
            "id=" + id +
            ", uid='" + uid + "'" +
            ", pinCode='" + pinCode + "'" +
            ", type='" + type + "'" +
            ", createdAt='" + createdAt + "'" +
            ", updatedAt='" + updatedAt + "'" +
            ", status='" + status + "'" +
            '}';
    }
}
