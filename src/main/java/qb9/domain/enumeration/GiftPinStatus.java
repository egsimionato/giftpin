package qb9.domain.enumeration;

/**
 * The GiftPinStatus enumeration.
 */
public enum GiftPinStatus {
    NEW,REDEEMED
}
