package qb9.domain.enumeration;

/**
 * The GiftPinType enumeration.
 */
public enum GiftPinType {
    BUY,FREE,REWARD
}
