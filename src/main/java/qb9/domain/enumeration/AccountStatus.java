package qb9.domain.enumeration;

/**
 * The AccountStatus enumeration.
 */
public enum AccountStatus {
    NEW,ACTIVE,BLOCKED
}
