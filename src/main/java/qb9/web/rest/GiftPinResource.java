package qb9.web.rest;

import com.codahale.metrics.annotation.Timed;
import qb9.service.GiftPinService;
import qb9.web.rest.util.HeaderUtil;
import qb9.web.rest.util.PaginationUtil;
import qb9.service.dto.GiftPinDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing GiftPin.
 */
@RestController
@RequestMapping("/api")
public class GiftPinResource {

    private final Logger log = LoggerFactory.getLogger(GiftPinResource.class);
        
    @Inject
    private GiftPinService giftPinService;

    /**
     * POST  /gift-pins : Create a new giftPin.
     *
     * @param giftPinDTO the giftPinDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new giftPinDTO, or with status 400 (Bad Request) if the giftPin has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/gift-pins",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<GiftPinDTO> createGiftPin(@RequestBody GiftPinDTO giftPinDTO) throws URISyntaxException {
        log.debug("REST request to save GiftPin : {}", giftPinDTO);
        if (giftPinDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("giftPin", "idexists", "A new giftPin cannot already have an ID")).body(null);
        }
        GiftPinDTO result = giftPinService.save(giftPinDTO);
        return ResponseEntity.created(new URI("/api/gift-pins/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("giftPin", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /gift-pins : Updates an existing giftPin.
     *
     * @param giftPinDTO the giftPinDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated giftPinDTO,
     * or with status 400 (Bad Request) if the giftPinDTO is not valid,
     * or with status 500 (Internal Server Error) if the giftPinDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/gift-pins",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<GiftPinDTO> updateGiftPin(@RequestBody GiftPinDTO giftPinDTO) throws URISyntaxException {
        log.debug("REST request to update GiftPin : {}", giftPinDTO);
        if (giftPinDTO.getId() == null) {
            return createGiftPin(giftPinDTO);
        }
        GiftPinDTO result = giftPinService.save(giftPinDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("giftPin", giftPinDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /gift-pins : get all the giftPins.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of giftPins in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/gift-pins",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<GiftPinDTO>> getAllGiftPins(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of GiftPins");
        Page<GiftPinDTO> page = giftPinService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/gift-pins");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /gift-pins/:id : get the "id" giftPin.
     *
     * @param id the id of the giftPinDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the giftPinDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/gift-pins/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<GiftPinDTO> getGiftPin(@PathVariable Long id) {
        log.debug("REST request to get GiftPin : {}", id);
        GiftPinDTO giftPinDTO = giftPinService.findOne(id);
        return Optional.ofNullable(giftPinDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /gift-pins/:id : delete the "id" giftPin.
     *
     * @param id the id of the giftPinDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/gift-pins/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteGiftPin(@PathVariable Long id) {
        log.debug("REST request to delete GiftPin : {}", id);
        giftPinService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("giftPin", id.toString())).build();
    }

}
