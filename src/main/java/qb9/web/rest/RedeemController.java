package qb9.web.rest;

import qb9.domain.Account;
import qb9.domain.GiftPin;
import qb9.repository.AccountRepository;
import qb9.repository.GiftPinRepository;
import qb9.service.RedeemService;
import qb9.service.AccountService;
import qb9.service.dto.AccountDTO;
import qb9.service.dto.RedeemDTO;

import qb9.web.rest.errors.ErrorDTO;
import qb9.web.rest.errors.ErrorConstants;

import java.lang.Math;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import io.swagger.annotations.*;
import com.codahale.metrics.annotation.Timed;

@Component
@RestController
@Transactional
public class RedeemController {

    private final Logger log = LoggerFactory.getLogger(RedeemController.class);

    @Inject
    private GiftPinRepository giftPinRepository;

    @Inject
    private AccountService accountService;

    @Inject
    private RedeemService redeemService;

    @Inject
    private AccountRepository accountRepository;


    /**
     * POST  account/unlock_random : Unlock a random
     *
     * @param UnlockComicDTO the unlock to processed.
     * @return the ResponseEntity with status 200 (Created) and with body the AccountInfoDTO information,
     * or with status 400 (Bad Request) if the username o comic are not registred,
     * or status 500 (Internal Server Error) if the unlock couldn't be done.
     *
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @ApiOperation(value = "Unlock a Random Comic",
            nickname = "unlock_random",
            notes = "Unlock a Random Comic by a player")
    @RequestMapping(value = "/api/accounts/{username}/redeem/{pinCode}",
            method = RequestMethod.POST)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Success", response = RedeemDTO.class),
        @ApiResponse(code = 400, message = "Invalid params"),
        @ApiResponse(code = 401, message = "Unauthorized"),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found"),
        @ApiResponse(code = 500, message = "Internal Server Error")})
    @Timed
    public ResponseEntity<?> redeem(
            @ApiParam(value="username", required=true) @PathVariable String username,
            @ApiParam(value="pinCode", required=true) @PathVariable String pinCode,
            @ApiParam(value="enable debug.", required=false) @RequestParam(value="debug", required=false) Optional<Boolean> isDebugEnabled,
            HttpServletResponse response) {
        log.debug("REST request to redeem {} pin for {} Account", pinCode, username);

        Account account = null;
        Optional<Account> oAccount = accountRepository.findOneByUsername(username);
        if (!oAccount.isPresent()) {
            log.debug("@Z Account no exists, process to create {}", username);
            account = accountService.save(accountService.create(username));
            log.debug("@Z Account for {} created", username);
        } else {
            account = oAccount.get();
        }

        RedeemDTO redeemDTO = null;
        Optional<RedeemDTO> oRedeemDTO = redeemService.redeem(account, pinCode);
        log.debug("@Z oDTO {}", oRedeemDTO);

        if (!oRedeemDTO.isPresent()) {
            ErrorDTO errorDTO = new ErrorDTO(
                     ErrorConstants.ERR_VALIDATION, ErrorConstants.ERR_VALIDATION);
            return new ResponseEntity<>(errorDTO, HttpStatus.BAD_REQUEST);
        }
        redeemDTO = oRedeemDTO.get();
        log.debug("@Z DTO {}", redeemDTO);

        if (redeemDTO.hasError()) {
            ErrorDTO errorDTO = new ErrorDTO(redeemDTO.getStatus().toString(), redeemDTO.getStatus().toString());
            return new ResponseEntity<>(redeemDTO, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(redeemDTO, HttpStatus.OK);
    }


}
