package qb9;

import qb9.config.Constants;
import qb9.domain.Account;
import qb9.domain.GiftPin;
import qb9.domain.enumeration.AccountStatus;
import qb9.domain.enumeration.GiftPinStatus;
import qb9.domain.enumeration.GiftPinType;
import qb9.repository.AccountRepository;
import qb9.repository.GiftPinRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.UUID;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PostConstruct;
import javax.inject.Inject;



@Configuration
@Profile("dev")
public class DataLoader {


    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));

    private static final String DEFAULT_CODE = "AAAAA";
    private static final String DEFAULT_EMAIL = "AAAAA";
    private static final String DEFAULT_SUMMARY = "AAAAA";
    private static final String DEFAULT_UID = "AAAAA";
    private static final String DEFAULT_USERNAME = "AAAAA";
    private static final String UPDATED_CODE = "BBBBB";
    private static final String UPDATED_EMAIL = "BBBBB";
    private static final String UPDATED_SUMMARY = "BBBBB";
    private static final String UPDATED_UID = "BBBBB";
    private static final String UPDATED_USERNAME = "BBBBB";


    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now();
    private static final String DEFAULT_CREATED_AT_STR = dateTimeFormatter.format(DEFAULT_CREATED_AT);
    private static final String DEFAULT_UPDATED_AT_STR = dateTimeFormatter.format(DEFAULT_UPDATED_AT);


    private static final String DEFAULT_TITLE = "AAAAA";
    private static final String UPDATED_TITLE = "BBBBB";
    private static final String DEFAULT_SAFE_TITLE = "AAAAA";
    private static final String UPDATED_SAFE_TITLE = "BBBBB";

    private static final Integer DEFAULT_NUMBER = 1;
    private static final Integer UPDATED_NUMBER = 2;
    private static final String DEFAULT_IMAGE_SRC = "AAAAA";
    private static final String UPDATED_IMAGE_SRC = "BBBBB";


    private final Logger log = LoggerFactory.getLogger(DataLoader.class);

    @Inject
    private AccountRepository accountRepository;

    @Inject
    private GiftPinRepository giftPinRepository;

    @Inject
    private Environment env;


    @PostConstruct
    public void loadData() {
        log.debug("Loading sample data v1");
        log.debug("now() => {}", ZonedDateTime.now());
        giftPinRepository.deleteAll();
        accountRepository.deleteAll();

        Account matildaAccount = fakeAccount("matilda");
        accountRepository.save(matildaAccount);
        Account virginiaAccount = fakeAccount("virginia");
        accountRepository.save(virginiaAccount);


        GiftPin matildaFirstPin = fakePin(matildaAccount);
        giftPinRepository.save(matildaFirstPin);

        GiftPin virginiaFirstPin = fakePin(matildaAccount);
        giftPinRepository.save(virginiaFirstPin);

        log.debug("Sample data loaded");
    }

    private Account fakeAccount() {
        Account account = new Account();
        account.setUid("giftpin.account."+UUID.randomUUID())
            .setEmail(DEFAULT_EMAIL)
            .setUsername(DEFAULT_USERNAME)
            .setCreatedAt(DEFAULT_CREATED_AT)
            .setUpdatedAt(DEFAULT_UPDATED_AT)
            .setStatus(AccountStatus.NEW);
        return account;
    }

    private Account fakeAccount(String username) {
        return fakeAccount()
            .setUsername(username)
            .setEmail(username+"@mail.com");
    }

    private GiftPin fakePin() {
        GiftPin giftPin = new GiftPin();
        UUID uuid =  UUID.randomUUID();
        giftPin.setUid("giftpin.giftpin."+uuid);
        giftPin.setPinCode(uuid.toString());
        giftPin.setType(GiftPinType.FREE);
        giftPin.setCreatedAt(UPDATED_UPDATED_AT);
        giftPin.setUpdatedAt(UPDATED_UPDATED_AT);
        giftPin.setStatus(GiftPinStatus.NEW);
        return giftPin;
    }

    private GiftPin fakePin(Account account) {
        GiftPin giftpin = fakePin();
        giftpin.setAccount(account);
        return giftpin;
    }


}
