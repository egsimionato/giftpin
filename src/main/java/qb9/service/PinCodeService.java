package qb9.service;

public interface PinCodeService {

    Boolean canUse(String pinCode);

    void burn(String pinCode);

}
