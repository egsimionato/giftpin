package qb9.service;

public interface BurnerService {

    public Boolean burn(String pinCode);
    public Boolean canUse(String pinCode);

}
