package qb9.service.mapper;

import qb9.domain.*;
import qb9.service.dto.GiftPinDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity GiftPin and its DTO GiftPinDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface GiftPinMapper {

    @Mapping(source = "account.id", target = "accountId")
    @Mapping(source = "account.username", target = "accountUsername")
    GiftPinDTO giftPinToGiftPinDTO(GiftPin giftPin);

    List<GiftPinDTO> giftPinsToGiftPinDTOs(List<GiftPin> giftPins);

    @Mapping(source = "accountId", target = "account")
    GiftPin giftPinDTOToGiftPin(GiftPinDTO giftPinDTO);

    List<GiftPin> giftPinDTOsToGiftPins(List<GiftPinDTO> giftPinDTOs);

    default Account accountFromId(Long id) {
        if (id == null) {
            return null;
        }
        Account account = new Account();
        account.setId(id);
        return account;
    }
}
