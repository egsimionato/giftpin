package qb9.service.mapper;

import qb9.domain.*;
import qb9.service.dto.AccountDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Account and its DTO AccountDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AccountMapper {

    AccountDTO accountToAccountDTO(Account account);

    List<AccountDTO> accountsToAccountDTOs(List<Account> accounts);

    Account accountDTOToAccount(AccountDTO accountDTO);

    List<Account> accountDTOsToAccounts(List<AccountDTO> accountDTOs);
}
