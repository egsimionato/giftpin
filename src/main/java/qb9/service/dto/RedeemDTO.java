package qb9.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import qb9.service.dto.RedeemStatus;
import qb9.service.dto.RewardResult;

/**
 * A DTO for the GiftPin entity.
 */
public class RedeemDTO implements Serializable {

    private String pinCode;

    private String username;

    private RedeemStatus status;

    private RewardResult reward;

    public RedeemDTO() {
         this.status = RedeemStatus.SUCCESS;
    }

    public static RedeemDTO SUCCESS() {
        RedeemDTO dto = new RedeemDTO();
        dto.status = RedeemStatus.SUCCESS;
        return dto;
    }

    public static RedeemDTO PREVIOUSLY_BURNED() {
        RedeemDTO dto = new RedeemDTO();
        dto.status = RedeemStatus.PREVIOUSLY_BURNED;
        return dto;
    }

    public static RedeemDTO PIN_NO_VALID() {
        RedeemDTO dto = new RedeemDTO();
        dto.status = RedeemStatus.PIN_NO_VALID;
        return dto;
    }

    public static RedeemDTO BURN_PIN_ERROR() {
        RedeemDTO dto = new RedeemDTO();
        dto.status = RedeemStatus.BURN_PIN_ERROR;
        return dto;
    }

    public String getPinCode() {
        return pinCode;
    }

    public RedeemDTO setPinCode(String pinCode) {
        this.pinCode = pinCode;
        return this;
    }
    public String getUsername() {
        return username;
    }

    public RedeemDTO setUsername(String username) {
        this.username = username;
        return this;
    }

    public RedeemStatus getStatus() {
        return status;
    }

    public RedeemDTO setStatus(RedeemStatus status) {
        this.status = status;
        return this;
    }

    public RewardResult getReward() {
        return reward;
    }

    public RedeemDTO setReward(RewardResult reward) {
        this.reward = reward;
        return this;
    }

    public Boolean hasError() {
        return this.status != RedeemStatus.SUCCESS;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RedeemDTO redeemDTO = (RedeemDTO) o;

        if ( ! Objects.equals(username, redeemDTO.username)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(username);
    }

    @Override
    public String toString() {
        return "RedeemDTO{" +
            "status='" + status + "'" +
            ", username='" + username + "'" +
            ", pinCode='" + pinCode + "'" +
            ", reward='" + reward + "'" +
            '}';
    }
}
