package qb9.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.Map;
import java.util.HashMap;

/**
 * A DTO for the Reward action.
 */
public class RewardResult implements Serializable {

    private Object note = "name:MATILDA_OK";
    private Object rewards = new HashMap<String, Integer>();

    public Object getNote() {
        return note;
    }

    public RewardResult setNote(Object note) {
        this.note = note;
        return this;
    }


    public Object getRewards() {
        return rewards;
    }

    public RewardResult setRewards(Object rewards) {
        this.rewards = rewards;
        return this;
    }

   // public RewardResult() {
   //     rewards.put("bola.de.nieve", 25);
   //     rewards.put("frasco.de.mutageno", 2);
   // }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RewardResult rewardResult = (RewardResult) o;

        if ( ! Objects.equals(rewards, rewardResult.rewards)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(rewards);
    }


    @Override
    public String toString() {
        return "RewardResult{" +
            ", rewards='" + rewards + "'" +
            ", note='" + note + "'" +
            '}';
    }
}
