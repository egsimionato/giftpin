package qb9.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import qb9.domain.enumeration.GiftPinType;
import qb9.domain.enumeration.GiftPinStatus;

/**
 * A DTO for the GiftPin entity.
 */
public class GiftPinDTO implements Serializable {

    private Long id;

    private String uid;

    private String pinCode;

    private GiftPinType type;

    private ZonedDateTime createdAt;

    private ZonedDateTime updatedAt;

    private GiftPinStatus status;


    private Long accountId;
    

    private String accountUsername;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getUid() {
        return uid;
    }

    public GiftPinDTO setUid(String uid) {
        this.uid = uid;
        return this;
    }
    public String getPinCode() {
        return pinCode;
    }

    public GiftPinDTO setPinCode(String pinCode) {
        this.pinCode = pinCode;
        return this;
    }
    public GiftPinType getType() {
        return type;
    }

    public GiftPinDTO setType(GiftPinType type) {
        this.type = type;
        return this;
    }
    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public GiftPinDTO setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }
    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public GiftPinDTO setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }
    public GiftPinStatus getStatus() {
        return status;
    }

    public GiftPinDTO setStatus(GiftPinStatus status) {
        this.status = status;
        return this;
    }

    public Long getAccountId() {
        return accountId;
    }

    public GiftPinDTO setAccountId(Long accountId) {
        this.accountId = accountId;
        return this;
    }


    public String getAccountUsername() {
        return accountUsername;
    }

    public GiftPinDTO setAccountUsername(String accountUsername) {
        this.accountUsername = accountUsername;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        GiftPinDTO giftPinDTO = (GiftPinDTO) o;

        if ( ! Objects.equals(id, giftPinDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "GiftPinDTO{" +
            "id=" + id +
            ", uid='" + uid + "'" +
            ", pinCode='" + pinCode + "'" +
            ", type='" + type + "'" +
            ", createdAt='" + createdAt + "'" +
            ", updatedAt='" + updatedAt + "'" +
            ", status='" + status + "'" +
            '}';
    }
}
