package qb9.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;

import qb9.domain.enumeration.AccountStatus;

/**
 * A DTO for the Account entity.
 */
public class AccountDTO implements Serializable {

    private Long id;

    private String uid;

    private String email;

    private String username;

    private ZonedDateTime createdAt;

    private ZonedDateTime updatedAt;

    private AccountStatus status;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getUid() {
        return uid;
    }

    public AccountDTO setUid(String uid) {
        this.uid = uid;
        return this;
    }
    public String getEmail() {
        return email;
    }

    public AccountDTO setEmail(String email) {
        this.email = email;
        return this;
    }
    public String getUsername() {
        return username;
    }

    public AccountDTO setUsername(String username) {
        this.username = username;
        return this;
    }
    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public AccountDTO setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }
    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public AccountDTO setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }
    public AccountStatus getStatus() {
        return status;
    }

    public AccountDTO setStatus(AccountStatus status) {
        this.status = status;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AccountDTO accountDTO = (AccountDTO) o;

        if ( ! Objects.equals(id, accountDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "AccountDTO{" +
            "id=" + id +
            ", uid='" + uid + "'" +
            ", email='" + email + "'" +
            ", username='" + username + "'" +
            ", createdAt='" + createdAt + "'" +
            ", updatedAt='" + updatedAt + "'" +
            ", status='" + status + "'" +
            '}';
    }
}
