package qb9.service;

import qb9.domain.Account;
import qb9.service.dto.RewardResult;

public interface RewarderService {

    RewardResult reward(Account account);

}
