package qb9.service.impl;

import qb9.service.BurnerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.Map;
import java.util.HashMap;

@Service
@Transactional
public class BurnerServiceImpl implements BurnerService {

    private final Logger log = LoggerFactory.getLogger(BurnerServiceImpl.class);

    @Value("${qb9.pines.canburn_url}")
    private String canBurnUrl;

    @Value("${qb9.pines.burn_url}")
    private String burnUrl;


    public Boolean burn(String pinCode) {
        RestTemplate rt = new RestTemplate();

        Map rVars = new HashMap();
        rVars.put("pinCode", pinCode);

        Map<String, Object> body = new HashMap();

        Boolean bResponse = rt.postForObject(
                burnUrl, body, Boolean.class, rVars);
        log.debug("@Z :::::::::::::::::::::: BURN PIN => {} :::::::", bResponse);
        return bResponse;
    }

    public Boolean canUse(String pinCode) {
        RestTemplate rt = new RestTemplate();

        Map rVars = new HashMap();
        rVars.put("pinCode", pinCode);

        Map<String, Object> body = new HashMap();

        Boolean bResponse = rt.postForObject(
                canBurnUrl, body, Boolean.class, rVars);
        log.debug("@Z :::::::::::::::::::::: CAN BURN ? => {} :::::::", bResponse);
        return bResponse;
    }

}
