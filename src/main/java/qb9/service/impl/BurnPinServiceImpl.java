package qb9.service.impl;

import qb9.service.BurnPinService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class BurnPinServiceImpl implements BurnPinService {

    private final Logger log = LoggerFactory.getLogger(BurnPinServiceImpl.class);

}
