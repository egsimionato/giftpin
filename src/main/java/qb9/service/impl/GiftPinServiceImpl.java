package qb9.service.impl;

import qb9.service.GiftPinService;
import qb9.domain.GiftPin;
import qb9.repository.GiftPinRepository;
import qb9.service.dto.GiftPinDTO;
import qb9.service.mapper.GiftPinMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing GiftPin.
 */
@Service
@Transactional
public class GiftPinServiceImpl implements GiftPinService{

    private final Logger log = LoggerFactory.getLogger(GiftPinServiceImpl.class);
    
    @Inject
    private GiftPinRepository giftPinRepository;

    @Inject
    private GiftPinMapper giftPinMapper;

    /**
     * Save a giftPin.
     *
     * @param giftPinDTO the entity to save
     * @return the persisted entity
     */
    public GiftPinDTO save(GiftPinDTO giftPinDTO) {
        log.debug("Request to save GiftPin : {}", giftPinDTO);
        GiftPin giftPin = giftPinMapper.giftPinDTOToGiftPin(giftPinDTO);
        giftPin = giftPinRepository.save(giftPin);
        GiftPinDTO result = giftPinMapper.giftPinToGiftPinDTO(giftPin);
        return result;
    }

    /**
     *  Get all the giftPins.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<GiftPinDTO> findAll(Pageable pageable) {
        log.debug("Request to get all GiftPins");
        Page<GiftPin> result = giftPinRepository.findAll(pageable);
        return result.map(giftPin -> giftPinMapper.giftPinToGiftPinDTO(giftPin));
    }

    /**
     *  Get one giftPin by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public GiftPinDTO findOne(Long id) {
        log.debug("Request to get GiftPin : {}", id);
        GiftPin giftPin = giftPinRepository.findOne(id);
        GiftPinDTO giftPinDTO = giftPinMapper.giftPinToGiftPinDTO(giftPin);
        return giftPinDTO;
    }

    /**
     *  Delete the  giftPin by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete GiftPin : {}", id);
        giftPinRepository.delete(id);
    }
}
