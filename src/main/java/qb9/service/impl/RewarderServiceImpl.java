package qb9.service.impl;

import qb9.domain.Account;
import qb9.service.dto.RewardResult;

import qb9.service.RewarderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.Map;
import java.util.HashMap;


@Service
@Transactional
public class RewarderServiceImpl implements RewarderService {

    private final Logger log = LoggerFactory.getLogger(RewarderServiceImpl.class);

    @Value("${qb9.reward.reward_choice_url}")
    private String rewardChoiceUrl;

    @Value("${qb9.reward.goal.giftpin}")
    private String goalCodeGiftPin;

    public RewardResult reward(Account account) {
        RestTemplate rt = new RestTemplate();


        Map rVars = new HashMap();

        Map<String, Object> body = new HashMap();
        body.put("goalCode", goalCodeGiftPin);
        body.put("playerUsername", account.getUsername());
        body.put("ratio", 1);

        Map<String, Object> mResponse = new HashMap();

        mResponse = rt.postForObject(rewardChoiceUrl, body, Map.class, rVars);
        log.debug("@Z :::::::::::::::::::::: REWARD GIFTPIN => {} :::::::", mResponse);

        return new RewardResult()
            .setNote(mResponse.get("more"))
            .setRewards(mResponse.get("rewards"));
    }
}
