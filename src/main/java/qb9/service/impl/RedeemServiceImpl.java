package qb9.service.impl;

import qb9.service.RedeemService;

import qb9.domain.Account;
import qb9.service.dto.RewardResult;
import qb9.service.dto.RedeemDTO;
import qb9.service.dto.RedeemStatus;
import qb9.service.BurnerService;
import qb9.service.RewarderService;

import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;

@Service
@Transactional
public class RedeemServiceImpl implements RedeemService {

    private final Logger log = LoggerFactory.getLogger(RedeemServiceImpl.class);

    @Inject
    BurnerService burner;

    @Inject
    RewarderService rewarder;

    public Optional<RedeemDTO> redeem(Account account, String pinCode) {
        log.debug("Request to perform a redeem a {} PinCode for {} Account",
                pinCode, account.getUsername());

        if (!burner.canUse(pinCode)) {
            log.debug("@Z {} Account will not be able to redeem the {} PinCode as this was previously burned",
                    account.getUsername(), pinCode);
            //return Optional.of(RedeemDTO.PREVIOUSLY_BURNED()
            return Optional.of(RedeemDTO.PIN_NO_VALID()
                .setUsername(account.getUsername())
                .setPinCode(pinCode));
        }

        RewardResult reward = rewarder.reward(account);

        Boolean isBurn = burner.burn(pinCode);
        if (!isBurn) {
            log.debug("@Z {} Account has a error to burn the {} PinCode",
                    account.getUsername(), pinCode);
            return Optional.of(RedeemDTO.BURN_PIN_ERROR()
                    .setUsername(account.getUsername())
                    .setPinCode(pinCode));
        }


        return Optional.of(RedeemDTO.SUCCESS()
                .setUsername(account.getUsername())
                .setPinCode(pinCode)
                .setReward(reward));
    }


}
