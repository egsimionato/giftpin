package qb9.service.impl;

import qb9.service.PinCodeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class PinCodeServiceImpl implements PinCodeService {

    private final Logger log = LoggerFactory.getLogger(PinCodeServiceImpl.class);

    public void burn(String pinCode) {
    }

    public Boolean canUse(String pinCode) {
        return false;
    }

}
