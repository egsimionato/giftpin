package qb9.service;

import qb9.service.dto.GiftPinDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing GiftPin.
 */
public interface GiftPinService {

    /**
     * Save a giftPin.
     *
     * @param giftPinDTO the entity to save
     * @return the persisted entity
     */
    GiftPinDTO save(GiftPinDTO giftPinDTO);

    /**
     *  Get all the giftPins.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<GiftPinDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" giftPin.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    GiftPinDTO findOne(Long id);

    /**
     *  Delete the "id" giftPin.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
