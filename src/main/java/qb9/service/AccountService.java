package qb9.service;

import qb9.domain.Account;
import qb9.service.dto.AccountDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing Account.
 */
public interface AccountService {

    /**
     * Save a account.
     *
     * @param accountDTO the entity to save
     * @return the persisted entity
     */
    AccountDTO save(AccountDTO accountDTO);

    /**
     * Save a account.
     *
     * @param account the entity to save
     * @return the persisted entity
     */
    Account save(Account account);

    /**
     *  Get all the accounts.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<AccountDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" account.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    AccountDTO findOne(Long id);

    /**
     *  Delete the "id" account.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Create a account by "username".
     * @param username the username of entity
     */
    Account create(String username);

}
