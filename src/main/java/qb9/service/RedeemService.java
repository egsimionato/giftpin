package qb9.service;

import qb9.domain.Account;
import qb9.service.dto.RedeemDTO;

import java.util.Optional;

public interface RedeemService {

    public Optional<RedeemDTO> redeem(Account account, String pinCode);

}
