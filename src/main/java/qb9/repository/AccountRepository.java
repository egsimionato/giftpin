package qb9.repository;

import qb9.domain.Account;

import org.springframework.data.jpa.repository.*;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the Account entity.
 */
@SuppressWarnings("unused")
public interface AccountRepository extends JpaRepository<Account,Long> {

    public Optional<Account> findOneByUsername(String username);

}
