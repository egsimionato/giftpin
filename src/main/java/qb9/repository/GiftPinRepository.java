package qb9.repository;

import qb9.domain.GiftPin;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the GiftPin entity.
 */
@SuppressWarnings("unused")
public interface GiftPinRepository extends JpaRepository<GiftPin,Long> {

}
