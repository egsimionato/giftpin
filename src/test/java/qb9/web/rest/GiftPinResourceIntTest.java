package qb9.web.rest;

import qb9.GiftpinApp;
import qb9.domain.GiftPin;
import qb9.repository.GiftPinRepository;
import qb9.service.GiftPinService;
import qb9.service.dto.GiftPinDTO;
import qb9.service.mapper.GiftPinMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import qb9.domain.enumeration.GiftPinType;
import qb9.domain.enumeration.GiftPinStatus;
/**
 * Test class for the GiftPinResource REST controller.
 *
 * @see GiftPinResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GiftpinApp.class)
public class GiftPinResourceIntTest {
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));
    private static final String DEFAULT_UID = "AAAAA";
    private static final String UPDATED_UID = "BBBBB";
    private static final String DEFAULT_PIN_CODE = "AAAAA";
    private static final String UPDATED_PIN_CODE = "BBBBB";

    private static final GiftPinType DEFAULT_TYPE = GiftPinType.BUY;
    private static final GiftPinType UPDATED_TYPE = GiftPinType.FREE;

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_CREATED_AT_STR = dateTimeFormatter.format(DEFAULT_CREATED_AT);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_UPDATED_AT_STR = dateTimeFormatter.format(DEFAULT_UPDATED_AT);

    private static final GiftPinStatus DEFAULT_STATUS = GiftPinStatus.NEW;
    private static final GiftPinStatus UPDATED_STATUS = GiftPinStatus.REDEEMED;

    @Inject
    private GiftPinRepository giftPinRepository;

    @Inject
    private GiftPinMapper giftPinMapper;

    @Inject
    private GiftPinService giftPinService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restGiftPinMockMvc;

    private GiftPin giftPin;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        GiftPinResource giftPinResource = new GiftPinResource();
        ReflectionTestUtils.setField(giftPinResource, "giftPinService", giftPinService);
        this.restGiftPinMockMvc = MockMvcBuilders.standaloneSetup(giftPinResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static GiftPin createEntity(EntityManager em) {
        GiftPin giftPin = new GiftPin();
        giftPin = new GiftPin();
        giftPin.setUid(DEFAULT_UID);
        giftPin.setPinCode(DEFAULT_PIN_CODE);
        giftPin.setType(DEFAULT_TYPE);
        giftPin.setCreatedAt(DEFAULT_CREATED_AT);
        giftPin.setUpdatedAt(DEFAULT_UPDATED_AT);
        giftPin.setStatus(DEFAULT_STATUS);
        return giftPin;
    }

    @Before
    public void initTest() {
        giftPin = createEntity(em);
    }

    @Test
    @Transactional
    public void createGiftPin() throws Exception {
        int databaseSizeBeforeCreate = giftPinRepository.findAll().size();

        // Create the GiftPin
        GiftPinDTO giftPinDTO = giftPinMapper.giftPinToGiftPinDTO(giftPin);

        restGiftPinMockMvc.perform(post("/api/gift-pins")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(giftPinDTO)))
                .andExpect(status().isCreated());

        // Validate the GiftPin in the database
        List<GiftPin> giftPins = giftPinRepository.findAll();
        assertThat(giftPins).hasSize(databaseSizeBeforeCreate + 1);
        GiftPin testGiftPin = giftPins.get(giftPins.size() - 1);
        assertThat(testGiftPin.getUid()).isEqualTo(DEFAULT_UID);
        assertThat(testGiftPin.getPinCode()).isEqualTo(DEFAULT_PIN_CODE);
        assertThat(testGiftPin.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testGiftPin.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testGiftPin.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testGiftPin.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void getAllGiftPins() throws Exception {
        // Initialize the database
        giftPinRepository.saveAndFlush(giftPin);

        // Get all the giftPins
        restGiftPinMockMvc.perform(get("/api/gift-pins?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(giftPin.getId().intValue())))
                .andExpect(jsonPath("$.[*].uid").value(hasItem(DEFAULT_UID.toString())))
                .andExpect(jsonPath("$.[*].pinCode").value(hasItem(DEFAULT_PIN_CODE.toString())))
                .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
                .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT_STR)))
                .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(DEFAULT_UPDATED_AT_STR)))
                .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }

    @Test
    @Transactional
    public void getGiftPin() throws Exception {
        // Initialize the database
        giftPinRepository.saveAndFlush(giftPin);

        // Get the giftPin
        restGiftPinMockMvc.perform(get("/api/gift-pins/{id}", giftPin.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(giftPin.getId().intValue()))
            .andExpect(jsonPath("$.uid").value(DEFAULT_UID.toString()))
            .andExpect(jsonPath("$.pinCode").value(DEFAULT_PIN_CODE.toString()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT_STR))
            .andExpect(jsonPath("$.updatedAt").value(DEFAULT_UPDATED_AT_STR))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingGiftPin() throws Exception {
        // Get the giftPin
        restGiftPinMockMvc.perform(get("/api/gift-pins/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateGiftPin() throws Exception {
        // Initialize the database
        giftPinRepository.saveAndFlush(giftPin);
        int databaseSizeBeforeUpdate = giftPinRepository.findAll().size();

        // Update the giftPin
        GiftPin updatedGiftPin = giftPinRepository.findOne(giftPin.getId());
        updatedGiftPin.setUid(UPDATED_UID);
        updatedGiftPin.setPinCode(UPDATED_PIN_CODE);
        updatedGiftPin.setType(UPDATED_TYPE);
        updatedGiftPin.setCreatedAt(UPDATED_CREATED_AT);
        updatedGiftPin.setUpdatedAt(UPDATED_UPDATED_AT);
        updatedGiftPin.setStatus(UPDATED_STATUS);
        GiftPinDTO giftPinDTO = giftPinMapper.giftPinToGiftPinDTO(updatedGiftPin);

        restGiftPinMockMvc.perform(put("/api/gift-pins")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(giftPinDTO)))
                .andExpect(status().isOk());

        // Validate the GiftPin in the database
        List<GiftPin> giftPins = giftPinRepository.findAll();
        assertThat(giftPins).hasSize(databaseSizeBeforeUpdate);
        GiftPin testGiftPin = giftPins.get(giftPins.size() - 1);
        assertThat(testGiftPin.getUid()).isEqualTo(UPDATED_UID);
        assertThat(testGiftPin.getPinCode()).isEqualTo(UPDATED_PIN_CODE);
        assertThat(testGiftPin.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testGiftPin.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testGiftPin.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testGiftPin.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void deleteGiftPin() throws Exception {
        // Initialize the database
        giftPinRepository.saveAndFlush(giftPin);
        int databaseSizeBeforeDelete = giftPinRepository.findAll().size();

        // Get the giftPin
        restGiftPinMockMvc.perform(delete("/api/gift-pins/{id}", giftPin.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<GiftPin> giftPins = giftPinRepository.findAll();
        assertThat(giftPins).hasSize(databaseSizeBeforeDelete - 1);
    }
}
